var store = {
  state: {
    login: false,
    username: '',
    currentGroup: '',
    groupList: [],
    messageList: [],
    readMessageList: [],
    unreadMessageList: [],
    errorMessage: '',
  }
}

Vue.component('login-screen', {
  data: function () {
    return {
      sharedState: store.state,
      textInput: ''
    }
  },
  template: `
    <div class="login-screen">
    <div>
      <h1>Enter your username</h1>
      <br>
      <input v-model="textInput" v-on:keyup.enter="login"/>
    </div>
    </div>
  `,
  methods: {
    login: function (event) {
      var obj = JSON.stringify({username: this.textInput})
      socket.emit('find_user', obj)
    }
  }
})

new Vue({
  el: '#app',
  data: { hello: 'Hello World!', message: '', sharedState: store.state },
  template: `
    <div id="app">
      <login-screen v-show="!this.sharedState.login"></login-screen>
      <div v-show="this.sharedState.login">
        <div id="left-side" ></div>
      </div>
      <div v-show="this.sharedState.login">
        <div id="right-side"></div>
      </div>
    </div>
  `
})


Vue.component('group-name', {
  props: {
    groupInfo: Object
  },
  computed: {
    showLetter: function () {
      return this.groupInfo.groupName.charAt(0).toUpperCase()
    }
  },
  template: /*html*/`
    <div class="group-name" v-on:click="switchGroup">
      <div class="circle mr-2"> {{this.showLetter}} </div>
      <h2 class="d-inline"> {{this.groupInfo.groupName}} </h2>
      <div class="line"></div>
    </div>
  `,
  methods: {
    switchGroup: function (event) {
      var obj = JSON.stringify({username: store.state.username, group_name: this.groupInfo.groupName})
      socket.emit('visit_group', obj)
    }
  }
})


Vue.component('chat-person', {
  props: {
    messageInfo: Object,
    read: String
  },
  computed: {
    showLetter: function () {
      return this.messageInfo.username.charAt(0).toUpperCase()
    },
    showDate: function() {
      var d = new Date(this.messageInfo.timestamp)
      return this.read + d.toLocaleString()
    },
    isUser: function() {
      return (this.messageInfo.username === store.state.username)
    }
  },
  template: /*html*/`
  <div>
    <div class="chat-person animated fadeInLeft" v-if="!isUser">
      <div class="circle chat-circle"> {{this.showLetter}} </div>
      <div class="chat-message d-inline-block"> {{this.messageInfo.text}} </div>
      <p class="notation mt-2">{{showDate}}</p>
    </div>
    <div class="chat-person me animated fadeInRight" v-if="isUser">
      <div class="chat-message-me d-inline-block"> {{this.messageInfo.text}} </div>
      <p class="notation mt-2">{{showDate}}</p>
    </div>
  </div>
  `
})
new Vue({
  el: '#left-side',
  data: { 
    textInputCreate: '', 
    textInputJoin: '',
    showModal: true, 
    sharedState: store.state 
  },
  template: /*html*/`
      <div id="left-side">
        <div id="left-side-real">
          <div class="top-bar animated fadeInLeft">
              <p style="display:inline;">Noomnim Chat 2</p>
              <div class="action-icon" style="display:inline-flex;">
                  <i class="fas white-icon" data-toggle="modal" data-target="#ModalCreate"></i>
                  <i class="fas white-icon" data-toggle="modal" data-target="#ModalJoin"></i>
                  <i class="fas white-icon" v-on:click="exitGroup"></i>
              </div>
          </div>
          <div class="group-list animated fadeInLeft">
              <group-name v-bind:groupInfo='group' v-for="group in this.sharedState.groupList" :key="group.groupName"/>
          </div>
        </div>
      
          <!-- Create Modal -->
          <div class="modal fade" id="ModalCreate" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="m-content ma-5">
                  <h4>Enter your new group name.</h4>
                  <h6 style="color:red;">{{this.sharedState.errorMessage}}</h6>
                  <input v-model="textInputCreate" class="mt-2 mb-3 my-input" v-on:keyup.enter="createGroup"></input>
                </div>
              </div>
            </div>
          </div>

          <!-- Join Modal -->
          <div class="modal fade" id="ModalJoin" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="m-content ma-5">
                  <h4>Enter group name that you wish to join.</h4>
                  <h6 style="color:red;">{{this.sharedState.errorMessage}}</h6>
                  <input v-model="textInputJoin" class="mt-2 mb-3 my-input" v-on:keyup.enter="joinGroup"></input>
                </div>
              </div>
            </div>
          </div>

          <!-- Error Modal -->
          <div class="modal fade show" id="ModalError" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="m-content ma-5">
                  <h4>{{this.sharedState.errorMessage}}</h4>
                </div>
              </div>
            </div>
          </div>

        </div>
    `,
  methods: {
    createGroup: function (event) {
      var obj = JSON.stringify({username: this.sharedState.username, group_name: this.textInputCreate})
      socket.emit('create_group', obj)
      this.textInputCreate = ''
    },
    joinGroup: function (event) {
      var obj = JSON.stringify({username: this.sharedState.username, group_name: this.textInputJoin})
      this.textInputJoin = ''
      socket.emit('join_group', obj)
    },
    exitGroup: function (event) {
      var obj = JSON.stringify({username: this.sharedState.username, group_name: this.sharedState.currentGroup})
      socket.emit('leave_group',obj)
    }
  }
})


new Vue({
  el: '#right-side',
  data: { hello: 'Hello World!', message: '', sharedState: store.state },
  template: /*html*/`
    <div class="mx-1">
        <div class="top-bar animated fadeInRight delay-05s">
            <h4 class="group-top"> {{this.sharedState.currentGroup}}
            <i v-if="this.sharedState.currentGroup" class="fas fa-comments"></i> 
            </h4>
        </div>
        <div class="chat-field animated fadeInRight delay-05s" id="chat-f">
            <chat-person v-bind:messageInfo='messageInfo' v-for="messageInfo in this.sharedState.readMessageList" :key="messageInfo.timestamp" read="Read "/>  
            <div class="line-long" v-if="this.sharedState.unreadMessageList.length > 0">New Messages</div>
            <chat-person v-bind:messageInfo='messageInfo' v-for="messageInfo in this.sharedState.unreadMessageList" :key="messageInfo.timestamp" read=""/>  
        </div>

        <div class="text-input animated fadeInUp delay-1s">
            <input id="message-input" class="form-control d-inline mr-2" placeholder="Enter text" v-model="message" v-on:keyup.enter="sendText"></input>
            <div class="send-btn" v-on:click="sendText"><i class="fa fa-paper-plane black-icon" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    `,
  methods: {
    sendText: function (event) {
      if(this.message != '') {
        var obj = JSON.stringify({username: this.sharedState.username, group_name: this.sharedState.currentGroup, text: this.message})
        socket.emit('send_message', obj)
        this.message = '';
      }
    }
  }
})

//bootstrap modal code
$('#ModalCreate').on('hide.bs.modal', function (e) {
  store.state.errorMessage = ''
})

$('#ModalJoin').on('hide.bs.modal', function (e) {
  store.state.errorMessage = ''
})

$('#ModalError').on('hide.bs.modal', function (e) {
  store.state.errorMessage = ''
})


//Done
socket.on('user_created', function(msg){
  store.state.username = msg.username
  store.state.login = true
})

//Done
socket.on('user_found', function(msg){
  store.state.username = msg.username
  var newList = []
  msg.group_list.forEach(function(element) {
    newList.push({'groupName':element})
  });
  store.state.groupList = newList;
  store.state.login = true

})

//Done
socket.on('leave_error', function(msg){
  store.state.errorMessage = msg.Error
  $('#ModalError').modal('toggle')
})

//Done
socket.on('leave_success', function(msg){
  var newList = []
  store.state.groupList.forEach(function(element) {
    if (element.groupName !== store.state.currentGroup) {
      newList.push(element)
    }
  })
  store.state.groupList = newList
  store.state.readMessageList = []
  store.state.unreadMessageList = []
  store.state.currentGroup = ''
})


//Done
socket.on('name_not_found', function(msg){
  store.state.errorMessage = msg.Error
})

//Done
socket.on('group_already_created', function(msg){
  store.state.errorMessage = msg.Error
})


socket.on('group_created', function(msg){ //TODO
  store.state.groupList.push({groupName: msg.group_name})
  $('#ModalCreate').modal('toggle')
  //switch to group automatically?
})

//Done
socket.on('join_success', function(msg){
  store.state.groupList.push({groupName: msg.group_name})
  $('#ModalJoin').modal('toggle')
})

//Done
socket.on('join_error', function(msg){
  store.state.errorMessage = msg.error
})

 //TODO
socket.on('enter_group', function(msg){
  //msg[0] is not read yet
  msg = JSON.parse(msg)
  var newList0 = msg[0]
  newList0.sort(function(a,b){
    var c = new Date(a.timestamp);
    var d = new Date(b.timestamp);
    return c-d;
  })
  store.state.unreadMessageList = newList0
  var newList1= msg[1]
  newList1.sort(function(a,b){
    var c = new Date(a.timestamp);
    var d = new Date(b.timestamp);
    return c-d;
  })
  store.state.readMessageList = newList1
  setTimeout(function(){  $("#chat-f").scrollTop($("#chat-f")[0].scrollHeight); }, 100);
  
})

socket.on('message_sent', function(msg){ //TODO
  msg = JSON.parse(msg)
  store.state.unreadMessageList.push({username:msg.username, timestamp:msg.timestamp, text:msg.text, groupName: msg.group_name})
  //push message into Chat
  //arrange it with timestamp
  var newList = store.state.readMessageList
  newList.sort(function(a,b){
    var c = new Date(a.timestamp);
    var d = new Date(b.timestamp);
    return c-d;
  })
  store.state.readMessageList = newList
  setTimeout(function(){  $("#chat-f").scrollTop($("#chat-f")[0].scrollHeight); }, 100);
  socket.emit("update_lastread", JSON.stringify({
    username: store.state.username
  }));
})

//Done
socket.on('already_in_the_group', function(msg){
  // store.state.errorMessage = msg.Error
  // $('#ModalError').modal('toggle')
})

//Done
socket.on('group_not_found', function(msg){
  store.state.errorMessage = msg.Error
  $('#ModalError').modal('toggle')
})

//Done
socket.on('user_visited', function(msg){
  store.state.currentGroup = msg.current_group
  var obj = JSON.stringify({username: store.state.username, group_name: store.state.currentGroup})
  socket.emit('enter_group', obj)
})
