var socket = io('http://localhost:3000')

console.log('check 1', socket.connected)

socket.on('connect', () => {
  console.log('Connected')
  console.log('check 2', socket.connected)
  if (store.state.currentGroup) {
    socket.emit('clear_session', JSON.stringify({
      username: store.state.username
    }))
  }
})

socket.on('pongg', message => {
  console.log(`from pong: ${message.message}`)
})

socket.on('clear_success', () => {
  var obj = JSON.stringify({
    username: store.state.username,
    group_name: store.state.currentGroup
  })
  socket.emit('visit_group', obj)
})
